# Formation e-artsup - Turn-based Roguelike

L'objectif est de réaliser un jeu de type roguelike au tour par tour. Ce qui vous est exigé dans cet exercice est volontairement lourd, et vous n'aurez pas le temps de tout faire. Néanmoins, vous devrez produire un prototype jouable, même s'il n'est pas terminé.

![Aperçu du rendu final du jeu](./Docs/Images/game-preview.png)

***NOTE* : Faites attention à votre architecture et à la propreté de votre code. Il se pourrait que ça compte, à un moment...**

## Scope

- Le jeu doit contenir un minimum de 3 niveaux, avec une difficulté croissante
- Le jeu doit contenir au moins 1 type d'ennemi
- Le jeu doit contenir au moins 1 items que le joueur peut ramasser et stocker
- Le jeu doit contenir au moins 1 bonus ponctuel

Si votre prototype fonctionne avec ce minimum exigé, vous pouvez intégrer des sons, particules et affiner l'UI (menu d'intro, credits, ...) afin de rendre votre prototype plus intéressant.

## Gameplay

Le **joueur** peut se déplacer sur une **map**, sous forme de grille.

Lorsque le joueur se déplace d'une case, il *joue un tour*. Son déplacement est alors résolu selon ce qu'il a en main, et l'entité présente sur la case vers laquelle il se déplace. Ensuite, toutes les autres entités présentes sur la grille vont agir à leur tour, selon des comportements simples détaillés dans la section "IA".

Le but du jeu est de traverser tous les niveaux. La partie est perdue dès que le joueur a 0 PV, auquel cas le jeu recommence du début.

### Caméra

Caméra orthographique. La map est vue de dessus, tandis que les personnages et les items sont vus de côté.

### Character

Le joueur a 2 statistiques :

- un nombre de PV. La quantité initiale et maximale est de 3. Si le nombre de PV tombe à 0, la partie est perdue.
- le Gold, qui augmente de 1 à chaque fois qu'un Gold est ramassé (voir "Bonus").

La seule action du joueur est de se déplacer d'une case horizontalement ou verticalement (ce qui correspond à un tour de jeu). Ce déplacement peut impliquer diverses intéractions, selon ce que le joueur possède et selon ce qui se trouve sur la case ciblée. A titre d'exemple, si le joueur se déplace vers une case vide, le déplacement est simplement appliqué. En revanche, s'il se déplace vers une case occupée par un ennemi, le joueur doit avoir une arme dans son inventaire pour remporter le combat, sans quoi il perd 1PV.

Le joueur dispose d'un inventaire basique pouvant contenir jusqu'à 2 **items**. Si le joueur doit ramasser un item alors que son inventaire est plein, l'objet n'est pas ramassés et reste disponible sur la map.

Plus d'informations sur les intéractions avec l'environnement dans la section "Résolution d'un tour".

### Controller

- ***Clavier***
  - **Se déplacer** : flèches directionnelles ou ZQSD
  - **Relancer la partie** : Echap
- ***Manette***
  - **Se déplacer** : Joystick gauche, droit ou D-Pad
  - **Relancer la partie** : Select

### Résolution d'un tour

Un tour de jeu est effectué de la part du joueur lorsqu'il se déplace vers une case. Ce déplacement implique plusieurs intéractions possibles, gérées dans cet ordre :

- **La case ciblée contient un ennemi** : si le joueur n'a pas d'arme dans son inventaire, il perd 1 PV et la résolution du déplacement est arrêtée. Sinon, l'ennemi est vaincu, et le déplacement continue de se résoudre normalement.
- **La case ciblée**
- **La case ciblée contient un item ou un bonus** : l'objet est ramassé, son effet est appliqué (voir "Collectibles"), et le déplacement continue de se résoudre normalement.
- **La case ciblée est vide** : le joueur se trouve maintenant sur la case ciblée.

## IA

Les IA du jeu doivent rester très simples. Elles resolvent leur comportement après chaque déplacement du joueur. Vous devez implémenter au moins l'IA "Ennemi basique" décrite ci-dessous. Les autres types d'ennemis décrits dans cette section sont facultatifs. Vous êtes également libres d'implémenter vos propres comportements.

### Ennemi basique

A chaque tour, cette IA va essayer de se rapprocher du joueur pour l'attaquer :

```
SI le joueur n'est pas sur une case adjacente
  Je m'approche d'une case vers lui
SINON
  Je l'attaque, il perd 1 PV
```

### Pick-pocket

A chaque tour, cette IA va essayer de prendre un objet de la map pour l'éloigner du joueur.

```
SI le joueur est sur une case adjacente
  Je m'éloigne d'une case de lui
SINON
  SI un objet se trouve sur ma case
    Je m'éloigne d'une case du joueur, et l'objet est également déplacé
  SINON SI un objet se trouve sur la map
    Je me rapproche d'une case vers l'objet
  SINON
    Je ne fais rien
```

### Ennemi robuste

A chaque tour, cette IA va patienter jusqu'à ce que le joueur soit suffisamment près pour aller l'attaquer. Cet ennemi peut prendre jusqu'à 2 dégâts avant d'être vaincu. Ce dernier est lent, et ne peut se déplacer que tous les 2 tours.

```
SI le joueur est sur une case adjacente
  SI mon "actionTurn" vaut 0
    "actionTurn" vaut 1
  SINON
    J'attaque le joueur, il perd 1 PV
    "actionTurn" vaut 0
SINON SI le joueur est à plus de 4 cases de moi
  Je me rapproche d'une case vers lui
SINON
  Je ne fais rien
```

## Gameplay Elements

### Next Level Mark

Lorsque le joueur atteint la case contenant le marqueur de niveau suivant (représenté par la flèche vers le haut sur l'aperçu du jeu), le niveau suivant est chargé OU le joueur gagne la partie s'il se trouve dans le dernier niveau.

### Collectibles

Les collectibles sont des objets présents sur la map pouvant être ramassés par le joueur lorsqu'il se déplace sur leur case. Il y a 2 types de collectibles : les **items** et les **bonus**.

#### Items

Les **items** sont placés dans l'inventaire du joueur lorsqu'ils sont ramassés. Ils sont utilisés automatiquement lors de la résolution d'un tour, et sont supprimés de l'inventaire dès qu'ils ont été utilisés.

- Arme (épée, pelle, ...) : Permet d'infliger 1 dégât à un ennemi
- Protection (bouclier, armure, ...) : Permet de prendre 1 dégat sans perdre de PV

#### Bonus

Les **bonus** sont utilisés dès qu'ils sont ramassés (plutôt que d'être placés dans l'inventaire).

- Soin (potion, nourriture, ...) : Restaure 1 ou plusieurs PV.
- Gold (pièces, trésor, ...) : Incrémente la stat de Gold de 1. Notez que le Gold peut être utilisé pour créer un challenge supplémentaire dans un niveau.

## Assets inclus

Pour réaliser ce projet, les packs listés ci-dessous sont inclus au projet. Vous êtes cependant libres d'intégrer d'autres assets extérieurs ou de réaliser les assets du jeu vous-même.

- https://www.kenney.nl/assets/pattern-pack
- https://www.kenney.nl/assets/particle-pack
- https://www.kenney.nl/assets/toon-characters-1
- https://www.kenney.nl/assets/map-pack
- https://www.kenney.nl/assets/game-icons